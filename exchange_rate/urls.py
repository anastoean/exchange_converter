from django.urls import path
from . import views

urlpatterns = [
    path('', views.exchange_rate, name='index'),
    path('result', views.result, name='result')
]