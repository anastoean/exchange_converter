from django.shortcuts import render
from .forms import ExchangeForm
import requests
from django.shortcuts import redirect
from django.http import HttpResponse
import json


def exchange_rate(request):
    forms = ExchangeForm()
    if request.POST:
        data = request.POST.dict()
        request.session['our_data'] = data
        return redirect('result')
    return render(request, 'index.html', {'forms': forms})

def result(request):

    #request parameters
    MAIN_URL = "https://v6.exchangerate-api.com/v6/"
    API_KEY = "fef42d82c7e40a1f72cffc6c"

    data = request.session['our_data']
    _from = data['From']
    to = data['To']
    amount = data['amount']
    FULL_URL = "{main_url}{api_key}/pair/{_from}/{to}/{amount}/".format(
       main_url=MAIN_URL,
       api_key=API_KEY,
       _from=_from,
       to=to,
       amount=amount
    )
    # import ipdb;
    # ipdb.set_trace()
    response = requests.get(FULL_URL)
    resp = response.json()
    res = resp['conversion_result']
    conversion_result = "Amount {amount} {_from} to {to} is: {result}".format(
        amount=amount,
        _from=_from,
        to=to,
        result=res
    )
    return HttpResponse(conversion_result)







